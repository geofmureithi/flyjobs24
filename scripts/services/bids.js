/**
 * Created by Ace on 05/11/2015.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .factory('BidService', BidService);

    BidService.$inject = ['$http'];
    function BidService($http) {
        var service = {};

        service.GetAll = GetAll;
        service.GetAllPending = GetAllPending;
        service.GetInProgress = GetInProgress;
        service.GetCompleted = GetCompleted;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;
        
        function GetAll() {
            return $http.get('/api/bids').then(handleSuccess, handleError('Error getting all bids'));
        }
        function GetAllPending() {
            return $http.get('/api/bids?state=pending').then(handleSuccess, handleError('Error getting all bids'));
        }
        function GetInProgress() {
            return $http.get('/api/bids?state=inprogress').then(handleSuccess, handleError('Error getting all bids'));
        }
        function GetCompleted() {
            return $http.get('/api/bids?state=completed').then(handleSuccess, handleError('Error getting all bids'));
        }

        function GetById(id) {
            return $http.get('/api/bids/?id=' + id).then(handleSuccess, handleError('Error getting bid by id'));
        }

        function Create(bid) {
            return $http.post('/api/bids/', bid).then(handleSuccess, handleError('Error creating bid'));
        }

        function Update(bid) {
            return $http.put('/api/bids/?id=' + bid.id, bid).then(handleSuccess, handleError('Error updating bid'));
        }

        function Delete(id) {
            return $http.delete('/api/bids/' + id).then(handleSuccess, handleError('Error deleting bid'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();