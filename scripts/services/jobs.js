/**
 * Created by Ace on 05/11/2015.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .factory('JobService', JobService);

    JobService.$inject = ['$http'];
    function JobService($http) {
        var service = {};

        service.GetAll = GetAll;
        service.GetAllPending = GetAllPending;
        service.GetInProgress = GetInProgress;
        service.GetCompleted = GetCompleted;
        service.GetAllOpen = GetAllOpen;
        service.GetById = GetById;
        service.GetJob = GetJob;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.UserCanBid = UserCanBid;

        return service;
        
        function GetAll() {
            return $http.get('/api/jobs').then(handleSuccess, handleError('Error getting all jobs'));
        }
        function GetAllPending() {
            return $http.get('/api/jobs?state=pending').then(handleSuccess, handleError('Error getting all jobs'));
        }
        function GetAllOpen() {
            return $http.get('/api/jobs?state=open').then(handleSuccess, handleError('Error getting all jobs'));
        }
        function GetInProgress() {
            return $http.get('/api/jobs?state=inprogress').then(handleSuccess, handleError('Error getting all jobs'));
        }
        function GetCompleted() {
            return $http.get('/api/jobs?state=completed').then(handleSuccess, handleError('Error getting all jobs'));
        }

        function GetById(id) {
            return $http.get('/api/jobs/?id=' + id).then(handleSuccess, handleError('Error getting job by id'));
        }
        function GetJob(id) {
            return $http.get('/api/jobs/?job=' + id).then(handleSuccess, handleError('Error getting job by id'));
        }

        function Create(job) {
            return $http.post('/api/jobs/', job).then(handleSuccess, handleError('Error creating job'));
        }

        function Update(job) {
            return $http.put('/api/jobs/' + job.id, job).then(handleSuccess, handleError('Error updating job'));
        }

        function Delete(id) {
            return $http.delete('/api/jobs/' + id).then(handleSuccess, handleError('Error deleting job'));
        }
        function UserCanBid(id){
            return $http.get('/api/jobs/?usercanbid=true&job=' + id).then(handleSuccess, handleError('Error getting job by id'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();