/**
 * Created by Ace on 05/11/2015.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .factory('QuestionService', QuestionService);

    QuestionService.$inject = ['$http'];
    function QuestionService($http) {
        var service = {};

        service.GetAll = GetAll;
        service.GetAllPending = GetAllPending;
        service.GetAnswered =  GetAnswered;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;


        return service;
        
        function GetAll() {
            return $http.get('/api/questions').then(handleSuccess, handleError('Error getting all questions'));
        }
        function GetAllPending() {
            return $http.get('/api/questions?state=pending').then(handleSuccess, handleError('Error getting all questions'));
        }
        function GetAnswered() {
            return $http.get('/api/questions?state=completed').then(handleSuccess, handleError('Error getting all questions'));
        }

        function GetById(id) {
            return $http.get('/api/questions/?id=' + id).then(handleSuccess, handleError('Error getting question by id'));
        }
        function Create(question) {
            return $http.post('/api/questions/', question).then(handleSuccess, handleError('Error creating question'));
        }

        function Update(question) {
            return $http.put('/api/questions/?id=' + question.id, question).then(handleSuccess, handleError('Error updating question'));
        }

        function Delete(id) {
            return $http.delete('/api/questions/' + id).then(handleSuccess, handleError('Error deleting question'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();