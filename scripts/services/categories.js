/**
 * Created by Ace on 05/11/2015.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .factory('CategoryService', CategoryService);

    CategoryService.$inject = ['$http'];
    function CategoryService($http) {
        var service = {};

        service.GetAll = GetAll;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;

        function GetAll() {
            return $http.get('/api/categories').then(handleSuccess, handleError('Error getting all categories'));
        }

        function Create(category) {
            return $http.post('/api/categories/', category).then(handleSuccess, handleError('Error creating category'));
        }

        function Update(category) {
            return $http.put('/api/categories/' + category.id, category).then(handleSuccess, handleError('Error updating category'));
        }

        function Delete(id) {
            return $http.delete('/api/categories/' + id).then(handleSuccess, handleError('Error deleting category'));
        }

        // private functions

        function handleSuccess(res) {

            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();