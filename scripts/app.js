(function () {
    'use strict';
 
    angular
        .module('app', ['ngRoute', 'ngCookies','ui.bootstrap','angularMoment','ui.router'])
        .config(config)
        .run(run);
    config.$inject = ['$stateProvider', '$urlRouterProvider','$locationProvider'];
    function config($stateProvider, $urlRouterProvider,$locationProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                controller: 'HomeController',
                templateUrl: '/scripts/views/home/main.html',
                controllerAs: 'vm',
                title: 'Home'
            })
 
            .state('login', {
                url: '/login',
                controller: 'LoginController',
                templateUrl: '/scripts/views/auth/login.html',
                controllerAs: 'vm',
                title: 'Login'
            })
 
            .state('register', {
                url: '/register',
                controller: 'RegisterController',
                templateUrl: '/scripts/views/auth/register.html',
                controllerAs: 'vm',
                title: 'Register'
            })
            .state('forgot-password', {
                url: '/forgot-password',
                controller: 'ForgotPasswordController',
                templateUrl: '/scripts/views/auth/forgotpassword.html',
                controllerAs: 'vm',
                title: 'Forgot Password'
            })
            .state('job', {
                url: '/job/{id:[0-9]{1,9}}',
                controller: 'JobsController',
                templateUrl: '/scripts/views/jobs/public.html',
                controllerAs: 'vm',
                title: 'View Job',
                resolve: {
                    jobs: function() {
                        return [];
                    }
                }
            })
            .state('account', {
                abstract: true,
                url: '/account',
                templateUrl: '/scripts/views/account/main.html'
            }).state('account.home', {
                url: '',
                controller: 'AccountController',
                templateUrl: '/scripts/views/account/home.html',
                controllerAs: 'vm',
                title: 'Account Home'
            }).state('account.questions', {
                abstract: true,
                url: '/questions',
                controller: 'QuestionsController',
                templateUrl: '/scripts/views/questions/main.html',
                resolve: {
                    questions: function(){
                        return;
                    }
                }
            }).state('account.questions.view', {
                url: '/view/{id:[0-9]{1,9}}',
                controller: 'QuestionsController',
                templateUrl: '/scripts/views/questions/view.html',
                controllerAs: 'vm',
                title: 'View Question',
                resolve: {
                    questions: function(){
                        return;
                    }
                }
            }).state('account.questions.pending', {
                url: '/pending',
                controller: 'QuestionsController',
                templateUrl: '/scripts/views/questions/list.html',
                controllerAs: 'vm',
                title: 'Unanswered Questions',
                resolve: {
                    questions: function(QuestionService) {
                        return QuestionService.GetAllPending();
                    }
                }
            }).state('account.questions.answered', {
                url: '/answered',
                controller: 'QuestionsController',
                templateUrl: '/scripts/views/questions/list.html',
                controllerAs: 'vm',
                title: 'Answered Questions',
                resolve: {
                    questions: function(QuestionService) {
                        return QuestionService.GetAnswered();
                    }
                }
            }).state('account.jobs', {
                abstract: true,
                url: '/jobs',
                templateUrl: '/scripts/views/jobs/main.html',
                title: 'View Jobs'
            }).state('account.jobs.home', {
                url: '',
                controller: 'JobsController',
                templateUrl: '/scripts/views/jobs/list.html',
                controllerAs: 'vm',
                title: 'Jobs List',
                resolve: {
                    jobs: function(JobService) {
                        return JobService.GetAll();
                    }
                }
            }).state('account.jobs.pending', {
                url: '/pending',
                controller: 'JobsController',
                templateUrl: '/scripts/views/jobs/list.html',
                controllerAs: 'vm',
                title: 'Jobs Pending Approval',
                resolve: {
                    jobs: function(JobService) {
                        return JobService.GetAllPending();
                    }
                }
            }).state('account.jobs.inprogress', {
                url: '/in-progress',
                controller: 'JobsController',
                templateUrl: '/scripts/views/jobs/list.html',
                controllerAs: 'vm',
                title: 'Jobs In Progress',
                resolve: {
                    jobs: function(JobService) {
                        return JobService.GetInProgress();
                    }
                }
            }).state('account.jobs.completed', {
                url: '/completed',
                controller: 'JobsController',
                templateUrl: '/scripts/views/jobs/list.html',
                controllerAs: 'vm',
                title: 'Completed Jobs',
                resolve: {
                    jobs: function(JobService) {
                        return JobService.GetCompleted();
                    }
                }
            }).state('account.jobs.add', {
                url: '/add',
                controller: 'JobsController',
                templateUrl: '/scripts/views/jobs/add.html',
                controllerAs: 'vm',
                title: 'Post New Job',
                resolve: {
                    jobs: function() {
                        return [];
                    }
                }
            }).state('account.jobs.success', {
                url: '/success/{id:[0-9]{1,9}}',
                controller: 'JobsController',
                templateUrl: '/scripts/views/jobs/success.html',
                controllerAs: 'vm',
                title: 'Job Posting Successful!',
                resolve: {
                    jobs: function() {
                        return [];
                    }
                }
            }).state('account.jobs.view', {
                url: '/view/{id:[0-9]{1,9}}',
                controller: 'JobsController',
                templateUrl: '/scripts/views/jobs/view.html',
                controllerAs: 'vm',
                title: 'View Job',
                resolve: {
                    jobs: function() {
                        return [];
                    }
                }
            }).state('account.jobs.edit', {
                url: '/edit/{id:[0-9]{1,9}}',
                controller: 'JobsController',
                templateUrl: '/scripts/views/jobs/edit.html',
                controllerAs: 'vm',
                title: 'Edit Job',
                resolve: {
                    jobs: function() {
                        return [];
                    }
                }
            }).state('account.bids', {
                abstract: true,
                url: '/bids',
                templateUrl: '/scripts/views/bids/main.html',
                title: 'View Bids'
            }).state('account.bids.home', {
                url: '',
                controller: 'JobsController',
                templateUrl: '/scripts/views/jobs/open.html',
                controllerAs: 'vm',
                title: 'All Possible Jobs',
                resolve: {
                    jobs: function(JobService) {
                        return JobService.GetAllOpen();
                    }
                }
            }).state('account.bids.pending', {
                url: '/pending',
                controller: 'BidsController',
                templateUrl: '/scripts/views/bids/list.html',
                controllerAs: 'vm',
                title: 'Bids Pending Approval',
                resolve: {
                    bids: function(BidService) {
                        return BidService.GetAllPending();
                    }
                }
            }).state('account.bids.inprogress', {
                url: '/in-progress',
                controller: 'BidsController',
                templateUrl: '/scripts/views/bids/list.html',
                controllerAs: 'vm',
                title: 'Bids In Progress',
                resolve: {
                    bids: function(BidService) {
                        return BidService.GetInProgress();
                    }
                }
            }).state('account.bids.completed', {
                url: '/completed',
                controller: 'BidsController',
                templateUrl: '/scripts/views/bids/list.html',
                controllerAs: 'vm',
                title: 'Completed Bids',
                resolve: {
                    bids: function(BidService) {
                        return BidService.GetCompleted();
                    }
                }
            }).state('account.bids.add', {
                url: '/add',
                controller: 'BidsController',
                templateUrl: '/scripts/views/bids/add.html',
                controllerAs: 'vm',
                title: 'Post New Bid',
                resolve: {
                    bids: function() {
                        return [];
                    }
                }
            }).state('account.bids.success', {
                url: '/success/{id:[0-9]{1,9}}',
                controller: 'BidsController',
                templateUrl: '/scripts/views/bids/success.html',
                controllerAs: 'vm',
                title: 'Bid Posting Successful!',
                resolve: {
                    bids: function() {
                        return [];
                    }
                }
            }).state('account.bids.view', {
                url: '/view/{id:[0-9]{1,9}}',
                controller: 'BidsController',
                templateUrl: '/scripts/views/bids/view.html',
                controllerAs: 'vm',
                title: 'Bid Posting Successful!',
                resolve: {
                    bids: function() {
                        return [];
                    }
                }
            }).state('account.bids.edit', {
                url: '/edit/{id:[0-9]{1,9}}',
                controller: 'BidsController',
                templateUrl: '/scripts/views/bids/edit.html',
                controllerAs: 'vm',
                title: 'Edit Bid',
                resolve: {
                    bids: function() {
                        return [];
                    }
                }
            });
        // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
        $urlRouterProvider

            // The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
            // Here we are just setting up some convenience urls.
            .when('/account?id', '/account')

            // If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
            .otherwise('/');

        $locationProvider.html5Mode( true );
    }
 
    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http','$state', '$stateParams'];
    function run($rootScope, $location, $cookieStore, $http, $state, $stateParams) {
        // keep user logged in after page refresh

        $rootScope.globals = $cookieStore.get('globals') || {};
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.app = { title : "FlyJobs24" };
        if ($rootScope.globals.currentUser) {
            //console.log($rootScope.globals.currentUser.authdata);
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }
        $rootScope.$on('$stateChangeSuccess', function (event, current) {

                $rootScope.Page = { title : current.title};
        });
 
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register','/forgot-password','/']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn){
                $location.path('/login');

            }


                $rootScope.isLoggedin = loggedIn?true:false;

        });
    }
 
})();
angular
    .module('app').filter('raw', ['$sce', function($sce){
    return function(val) {
        return $sce.trustAsHtml(val);
    };
}]);