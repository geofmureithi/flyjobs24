(function () {
    'use strict';

    angular
        .module('app')
        .controller('AccountController', AccountController);

    AccountController.$inject = ['$location','UserService', '$rootScope','CategoryService','JobService','FlashService'];
    function AccountController($location, UserService, $rootScope, CategoryService, JobService,FlashService) {
        var vm = this;
        vm.isSettingsCollapsed = true;
        vm.isPreferencesCollapsed = true;
        vm.user = $rootScope.globals.currentUser.user;
        vm.updateDetails = updateDetails;


        function updateDetails(){

        }

        function loadAccountDetails() {

        }

        function deleteUser(id) {
            UserService.Delete(id)
            .then(function () {
                loadAllUsers();
            });
        }
    }

})();