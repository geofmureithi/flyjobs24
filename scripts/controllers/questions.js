(function () {
    'use strict';

    angular
        .module('app')
        .controller('QuestionsController', QuestionsController);

    QuestionsController.$inject = ['$location', '$rootScope','FlashService','QuestionService','$stateParams','$modal','questions'];
    function QuestionsController($location, $rootScope,FlashService, QuestionService,$stateParams ,$modal,questions) {
        var vm = this;
        vm.questions = questions;
        vm.answermodal = function (questionId) {
                        var modalInstance = $modal.open({
                            animation: true,
                            templateUrl: 'AnswerModalContent.html',
                            controller: 'AnswerInstanceCtrl',
                            size: 'lg',
                            resolve: {
                                question: function(){
                                   return questionId ;
                                }
                            }
                        });


        };
        if($stateParams.id){
            loadQuestionsDetails($stateParams.id)
        }
        function loadQuestionsDetails(id) {
            QuestionService.GetById(id).then(
                function(responce){
                    if(responce.success){
                        vm.question = responce.question;
                    }else{
                        $location.path('/account/');
                    }
                });
        }


    }

})();

angular.module('app').controller('AnswerInstanceCtrl',AnswerInstanceCtrl);
AnswerInstanceCtrl.$inject = ['$scope','$modalInstance','question','FlashService','QuestionService'];
function AnswerInstanceCtrl($scope, $modalInstance, question,FlashService,QuestionService) {
    $scope.question = {};
    $scope.question.id = question;

    $scope.ok = function () {
        QuestionService.Update($scope.question).then(function (response) {
            if(response.success){
                FlashService.Success(response.message, true);
                $modalInstance.close();
            }else {
                FlashService.Error(response.message);
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}