(function () {
    'use strict';
 
    angular
        .module('app')
        .controller('ForgotPasswordController', ForgotPasswordController);

    ForgotPasswordController.$inject = ['UserService', '$location', '$rootScope', 'FlashService'];
    function ForgotPasswordController(UserService, $location, $rootScope, FlashService) {
        var vm = this;
 
        vm.recover = recover;
 
        function recover() {
            vm.dataLoading = true;
            UserService.Recover(vm.user)
                .then(function (response) {
                    if (response.success) {
                        FlashService.Success('Please check your email for details on how to recover your account', true);
                        $location.path('/login');
                    } else {
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                    }
                });
        }
    }
 
})();