(function () {
    'use strict';

    angular
        .module('app')
        .controller('JobsController', JobsController);

    JobsController.$inject = ['$location','UserService', '$rootScope','CategoryService','JobService','FlashService','$stateParams','$modal','jobs'];
    function JobsController($location, UserService, $rootScope, CategoryService, JobService,FlashService,$stateParams ,$modal, jobs) {
        var vm = this;
        vm.isSettingsCollapsed = true;
        vm.isPreferencesCollapsed = true;
        vm.updateDetails = updateDetails;
        vm.addJob = addJob;
        vm.jobs = jobs;
        vm.openbidform = function (size) {
            if(JobService.UserCanBid($stateParams.id).then(function(response){
                    if(response.success){
                        var modalInstance = $modal.open({
                            animation: true,
                            templateUrl: 'myModalContent.html',
                            controller: 'BidFormModalInstanceCtrl',
                            size: size,
                            resolve: {
                                job: function () {
                                    return $stateParams.id;
                                }

                            }
                        });
                    }else{
                        FlashService.Error(response.message);
                    }
                }));

        };
        vm.openbidhandler = function (bid,status) {

                        var modalInstance = $modal.open({
                            animation: true,
                            templateUrl: 'myModalContent.html',
                            controller: 'QuesCtrl',
                            size: 'lg',
                            resolve: {
                                'bid': function(){
                                    return bid;
                                },
                                'status': function(){
                                    return status;
                                }

                            }
                        });


        };
        vm.openquestionform = function (size) {

                        var modalInstance = $modal.open({
                            animation: true,
                            templateUrl: 'QuestionModalContent.html',
                            controller: 'QuestionModalCtrl',
                            size: size,
                            resolve: {
                                'job': function(){
                                    return $stateParams.id;
                                }

                            }
                        });


        };


        initController();
        if($stateParams.id && $rootScope.$state.includes('job')){
            viewJob($stateParams.id)
        }else if($stateParams.id){
            loadJobDetails($stateParams.id)
        }



        function initController() {
            CategoryService.GetAll()
                .then(function (response) {
                    vm.categories = response
                });
            //$scope.contact = utils.findById($scope.contacts, $stateParams.contactId);

        }
        function updateDetails(){

        }
        function addJob() {
            vm.dataLoading = true;
            JobService.Create(vm.job)
                .then(function (response) {
                    if (response.success) {
                        FlashService.Success('Job Successfully Created', true);
                        $location.path('/account/jobs/success/'+response.id);
                    } else {
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                    }
                });
        }
        function loadJobDetails(id) {
            JobService.GetById(id).then(
                function(responce){
                    if(responce.success){
                        vm.job = responce.job;
                    }else{
                        $location.path('/account/jobs/');
                    }
                });
        }
        function viewJob(id) {
            JobService.GetJob(id).then(
                function(responce){
                        vm.job = responce.job;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
            .then(function () {
                loadAllUsers();
            });
        }
    }

})();

angular.module('app').controller('BidFormModalInstanceCtrl',BidFormModalInstanceCtrl);
BidFormModalInstanceCtrl.$inject = ['$scope','$modalInstance','job',  'BidService','FlashService'];
function BidFormModalInstanceCtrl($scope, $modalInstance, job, BidService,FlashService) {
    $scope.bid = {};
    $scope.bid.job = job;
    $scope.ok = function () {
        BidService.Create($scope.bid).then(function (response) {
            if(response.success){
                FlashService.Success(response.message, true);
                $modalInstance.close();
            }else {
                FlashService.Error(response.message);
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}
angular.module('app').controller('QuestionModalCtrl',QuestionModalCtrl);
QuestionModalCtrl.$inject = ['$scope','$modalInstance','job',  'QuestionService','FlashService'];
function QuestionModalCtrl($scope, $modalInstance, job, QuestionService,FlashService) {
    $scope.question = {};
    $scope.question.job = job;
    $scope.ok = function () {
        QuestionService.Create($scope.question).then(function (response) {
            if(response.success){
                FlashService.Success(response.message, true);
                $modalInstance.close();
            }else {
                FlashService.Error(response.message);
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}
angular.module('app').controller('BidHandlerModalInstanceCtrl',BidHandlerModalInstanceCtrl);
BidHandlerModalInstanceCtrl.$inject = ['$scope','$modalInstance','bid',  'status','FlashService','BidService'];
function BidHandlerModalInstanceCtrl($scope, $modalInstance, bid, status,FlashService,BidService) {
    $scope.bid = {id: bid};
    $scope.title = status;
    if(status == 'Approve'){
        $scope.bid.bid_accepted_date = new Date().toISOString();
        $scope.bid.active = 1;
    }else{
        $scope.bid.bid_rejected_date = new Date().toISOString();
        $scope.bid.active = 0;
    }
    $scope.ok = function () {
        console.log($scope.bid);
        BidService.Update($scope.bid).then(function (response) {
            if(response.success){
                FlashService.Success(response.message, true);
                $modalInstance.close();
            }else {
                FlashService.Error(response.message);
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}