(function () {
    'use strict';

    angular
        .module('app')
        .controller('BidsController', BidsController);

    BidsController.$inject = ['$location','UserService', '$rootScope','CategoryService','BidService','FlashService','$stateParams','bids'];
    function BidsController($location, UserService, $rootScope, CategoryService, BidService,FlashService,$stateParams, bids) {
        var vm = this;
        vm.isSettingsCollapsed = true;
        vm.isPreferencesCollapsed = true;
        vm.updateDetails = updateDetails;
        vm.addBid = addBid;
        vm.bids = bids;

        initController();
        if($stateParams.id)
            loadBidDetails($stateParams.id)

        function initController() {

        }
        function updateDetails(){

        }
        function addBid() {
            vm.dataLoading = true;
            BidService.Create(vm.bid)
                .then(function (response) {
                    if (response.success) {
                        FlashService.Success('Bid Successfully Created', true);
                        $location.path('/account/bids/success/'+response.id);
                    } else {
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                    }
                });
        }
        function loadBidDetails(id) {
            BidService.GetById(id).then(
                function(responce){
                    if(responce.success){
                        vm.bid = responce.bid;
                    }else{
                        $location.path('/account/bids/');
                    }
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
            .then(function () {
                loadAllUsers();
            });
        }
    }

})();