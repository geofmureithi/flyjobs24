<?php
namespace Admin\Controllers;
use Admin\Models\User;

class Home extends Controller{
    function index(\Base $f3)
    {
        $f3->set('INNER.PAGE', 'dashboard/index.html');
        $f3->set('page.title', 'Dashboard');
    }
}