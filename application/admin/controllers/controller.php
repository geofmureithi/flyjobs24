<?php
namespace Admin\Controllers;


class Controller extends \BaseController {
    protected $limit = 10;
    function beforeroute($f3,$params) {
        //code here;

        if(!\Admin\Controllers\Auth::isLoggedIn())
            $f3->reroute('/auth/login');
        if(!\Admin\Controllers\Auth::isAdmin())
            $f3->error('404','Not Found HTTP GET /admin');
        $f3 = \Base::instance();
        $f3->set('UI','application/admin/views/');
        $f3->set('module','admin');

        $this->page = \Pagination::findCurrentPage();
        $this->option = array('order' => 'datecreated DESC');
        $this->filter = null;
        parent::beforeroute($f3,$params);

    }
    function index(\Base $f3) {
        $f3->set('list',$this->model->find());
        if($_GET['filter'] == 'inactive' )
            $f3->set('list',$this->model->find(array('active = ?', 0)));
        if($_GET['filter'] == 'active')
            $f3->set('list',$this->model->find(array('active = ?', 1)));
        $f3->set('INNER.PAGE',$this->prural.'/listing.html');
        $f3->set('page.title','Listing '.ucfirst($this->prural));
        if($_GET['filter'])
            $f3->set('page.title','Listing '.ucfirst($_GET['filter']). ' ' .ucfirst($this->prural));

    }
    function add($f3) {

        if($f3->exists('POST.'.$this->singular)) {
            $this->model->reset();
            $this->model->copyfrom('POST.'.$this->singular);
            $this->model->save();
            \Flash::instance()->addMessage('New '. ucfirst($this->singular). ' Successfully added to Database','success');
            $f3->reroute('/admin/' .$this->prural);
        }
        $f3->set('INNER.PAGE',$this->prural.'/add.html');
        $f3->set('page.title','Add '. ucfirst($this->singular));

    }
    function edit($f3, $params) {

        $this->model->reset();
        $this->model->load(array('id = ?', $params['id']));
        if($f3->exists('POST.'.$this->singular)) {
            $this->model->copyfrom('POST.'.$this->singular);
            $this->model->save();
            \Flash::instance()->addMessage(ucfirst($this->singular). ' #'.$this->model->id .' Successfully Edited','success');
            $f3->reroute('/admin/' .$this->prural);
        }
        $this->model->copyto('POST.'.$this->singular);
        $f3->set('INNER.PAGE',$this->prural.'/add.html');
        $f3->set('page.title','Edit '. ucfirst($this->singular) . '#'.$this->model->id);

    }
    function view($f3, $params) {
        $this->model->reset();
        $this->model->load(array('id = ?', $params['id']));
        $this->model->copyto($this->singular);
        $f3->set('INNER.PAGE',$this->prural.'/view.html');
        $f3->set('page.title','View '. ucfirst($this->singular) . '#'.$this->model->id);

    }
    function upload($f3,$params){
        $type = $f3->get('POST.fileType');
        switch($type){
            case "image":
                $f3->set('UPLOADS', 'files/'.$this->prural.'/');
                break;
        }
        $overwrite = false; // set to true, to overwrite an existing file; Default: false
        $slug = true; // rename file to filesystem-friendly version
        $files = \Web::instance()->receive(function($file,$formFieldName){

                return true;
            },
            $overwrite,
            function($fileBaseName, $formFieldName){
                // build new file name from base name or input field name
                return time(). '_' .$fileBaseName;
            }
        );
        foreach($files as $key=>$val){
            if($val===true)
                $this->view->data =  array('fileName'=>str_replace('files/','',$key));
        }

    }
    function delete($f3, $params){

        $this->model->reset();
        $this->model
            ->load(array('id = ?',$params['id']));

        try{
            $this->model->erase();
            \Flash::instance()->addMessage(ucfirst($this->singular) . " #".$params['id']." Successfully Deleted",'');
        }
        catch (\Exception $e){
            \Flash::instance()->addMessage($e->getMessage(),'error');
        }
        $f3->reroute('/' . $f3->get('PARAMS.app'). '/' . $this->prural. '/index');
    }

    function afterroute($f3,$params) {
        //code here;
        parent::afterroute($f3,$params);

    }
}