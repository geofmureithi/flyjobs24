<?php
namespace Admin\Controllers;


class UserTypes extends Controller {
    function __construct(){
        $this->model = new \Admin\Models\Usertype();
        $this->singular = 'usertype';
        $this->prural = 'usertypes';
    }


}