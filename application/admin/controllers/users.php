<?php
namespace Admin\Controllers;


class Users extends Controller {
    function __construct(){
        $this->model = new \Admin\Models\User();
        $this->singular = 'user';
        $this->prural = 'users';
    }


}