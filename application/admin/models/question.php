<?php
namespace Admin\Models;

class Question extends \BaseModel {
    protected $table ="questions";
	protected $fieldConf = array(
		'title'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'job'=> array(
            'belongs-to-one'=>'\Admin\Models\Job'
        ),
        'user'=>array(
            'belongs-to-one' => '\Admin\Models\User',
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'answer'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
		'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
	);
	static function install(){
		echo "Create Questions Table<br/>" . PHP_EOL;
		static::setup();
	}




}