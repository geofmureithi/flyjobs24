<?php
namespace Admin\Models;

class Setting extends \BaseModel {
    protected $table ="settings";
    public  $list = ['site.title','site.description','site.address', 'site.email', 'smtp.host','smtp.port','smtp.scheme','smtp.user','smtp.password'];
    protected $fieldConf = array(
        'name'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'value'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
        'creator' => array(
            'belongs-to-one' => '\Admin\Models\User',
        ),
        'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
    );
    static function reload(\Base $f3){
        $settings = (new static)->find(null)?(new static)->find(null):array();
        foreach($settings as $v){
            $f3->set($v->name, $v->value);
        }
    }
}