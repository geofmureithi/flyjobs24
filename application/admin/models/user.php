<?php
namespace Admin\Models;

class User extends \BaseModel {
    protected $table ="users";
	protected $fieldConf = array(
        'firstname'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
		'lastname'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'email'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
		'password'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
		'companyname'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'gender'=>array(
            'type' => \DB\SQL\Schema::DT_INT2,
        ),
		'streetname'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
		'housenumber'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
		'zip'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
		'city'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
		'country'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
		'insurancecompanyname'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
		'phonenumber'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
		'homepointaddress'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
		'actionradius'=>array(
            'type' => \DB\SQL\Schema::DT_INT4,
        ),
		'experiencelevel'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
		'skilllevel'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR128,
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
		'usertypeagent' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
		'usertypeclient' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        
        'tou_accepted_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
		'datelastlogin' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
		'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
	);
	static function install(){
		echo "Create User Table";
		static::setup();
	}
    function __construct(){
        parent::__construct();
        $this->beforeerase(function($self){
            
        });
    }
    function set_password($value){
        $repeat = \Base::instance()->get('POST.password_repeat');
        if(!empty($value) && $value === $repeat){
            \Flash::instance()->addMessage('Password For User #' .$this->id .' Changed', 'warning');
            return \Bcrypt::instance()->hash($value,\Base::instance()->get('HASH'));
        } elseif(!empty($repeat)) {
            \Flash::instance()->addMessage('Password Change Failed. Please try again', 'warning');
        }
        return $this->password;
    }




}