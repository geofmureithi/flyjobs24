<?php
namespace Admin\Models;

class Category extends \BaseModel {
    protected $table ="categories";
	protected $fieldConf = array(
		'title'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
		'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
	);
	static function install(){
		echo "Create Categories Table<br/>" . PHP_EOL;
		static::setup();
	}




}