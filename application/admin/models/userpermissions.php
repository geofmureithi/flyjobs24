<?php
namespace Admin\Models;

class Userpermissions extends \BaseModel {
    protected $table ="user_permissions";
    public $permissions = [
        'users'=>['index','add','edit','delete'],
        'books'=>['index','add', 'edit','delete','upload'],
        'categories'=>['index','add', 'edit','delete','upload'],
        'home'=>['index','exportBooks','exportSoldBooks'],
        'orders'=> ['index'],
        'usertypes'=>['index','add','edit','delete'],
    ];
    protected $fieldConf = [
        'userstype' => array(
            'belongs-to-one' => array('\Project\Admin\Usertype','usertype'),
        ),
    ];

}