<?php
namespace Admin\Models;

class Job extends \BaseModel {
    protected $table ="jobs";
	protected $fieldConf = array(
        'title'=>array( #Any job requires title right???
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'user'=>array(
            'belongs-to-one' => '\Admin\Models\User',
        ),
		'category'=>array(
            'belongs-to-one' => '\Admin\Models\Category',
        ),
        'bids'=>array(
            'has-many' => array('\Admin\Models\Bid','job'),
        ),
        'description'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
		'address'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
        'typeofexecutiondate'=>array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
        ),
		'budget'=>array(
            'type' => \DB\SQL\Schema::DT_INT8,
        ),
        'execution_due_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'execution_earliest_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'execution_latest_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'tou_accepted_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'job_created_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'bid_accepted_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
		'job_finished_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
		'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
	);
	static function install(){
		echo "Create Jobs Table<br/>" . PHP_EOL;
		static::setup();
	}




}