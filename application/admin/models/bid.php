<?php
namespace Admin\Models;

class Bid extends \BaseModel {
    protected $table ="jobbid";
	protected $fieldConf = array(
        'user'=>array(
            'belongs-to-one' => '\Admin\Models\User',
        ),
		'job'=>array(
            'belongs-to-one' => '\Admin\Models\Job',
        ),
        'message'=>array(
            'type' => \DB\SQL\Schema::DT_TEXT,
        ),
		'amount'=>array(
            'type' => \DB\SQL\Schema::DT_INT8,
        ),
        'tou_accepted_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'active' => array(
            'type' => \DB\SQL\Schema::DT_BOOL,
            'default'=> 0
        ),
        'client_message' => array(
            'type'=> \DB\SQL\Schema::DT_TEXT
        ),
        'bid_accepted_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'bid_declined_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
        'bid_completed_date' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
        ),
		'datecreated' => array(
            'type' => \DB\SQL\Schema::DT_TIMESTAMP,
            'default' => \DB\SQL\Schema::DF_CURRENT_TIMESTAMP,
        ),
	);
	static function install(){
		echo "Create Bids Table<br/>" . PHP_EOL;
		static::setup();
	}




}