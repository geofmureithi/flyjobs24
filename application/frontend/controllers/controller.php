<?php
namespace Frontend\Controllers;


class Controller extends \BaseController {
    protected $limit = 10;
    function beforeroute($f3,$params) {
        //code here;
        $f3 = \Base::instance();
        $f3->set('UI','application/frontend/views/');
        parent::beforeroute($f3,$params);

    }
    function index(\Base $f3){}
    
    function upload($f3,$params){
        $type = $f3->get('POST.fileType');
        switch($type){
            case "image":
                $f3->set('UPLOADS', 'files/');
                break;
        }
        $overwrite = false; // set to true, to overwrite an existing file; Default: false
        $slug = true; // rename file to filesystem-friendly version
        $files = \Web::instance()->receive(function($file,$formFieldName){

                return true;
            },
            $overwrite,
            function($fileBaseName, $formFieldName){
                // build new file name from base name or input field name
                return time(). '_' .$fileBaseName;
            }
        );
        foreach($files as $key=>$val){
            if($val===true)
                $this->view->data =  array('fileName'=>str_replace('files/','',$key));
        }

    }

    function afterroute($f3,$params) {
        //code here;
        parent::afterroute($f3,$params);

    }
}