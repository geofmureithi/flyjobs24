<?php
/**
 * Created by PhpStorm.
 * User: Ace
 * Date: 01/11/2015
 * Time: 07:16
 */

namespace Api;


use Admin\Models\User;

class Users extends Base {
    function get(\Base $f3,$params) {
        echo 'get';
    }
    function post( \Base $f3,$params) {
        $data = $f3->get('POST');
        $user = (new User())->load(array('email = ?', $f3->get('POST.email')));
        if($user->dry()){
            $user->copyfrom('POST',$user->fields());
            if($data['usertype'] == 'agent'){
                $user->usertypeagent = 1;
            } else {
                $user->usertypeclient = 1;
            }
            $user->save();
            $this->view->data = ['success' => true, 'message' => 'Account created Successfully'];
        }else{
            $this->view->data = ['success' => false, 'message' => 'Account not created. Email already Exists. Please login.'];
        }

    }
    function put(\Base $f3,$params) {
         echo 'put';
    }
    function delete(\Base $f3,$params) {
         echo 'delete';
    }

} 