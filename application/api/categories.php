<?php
/**
 * Created by PhpStorm.
 * Category: Ace
 * Date: 01/11/2015
 * Time: 07:16
 */

namespace Api;


use Admin\Models\Category;

class Categories extends Base {
    function get(\Base $f3,$params) {
        $categories = (new Category())->find();
        $this->view->data = [];
        if($categories)
            $this->view->data = $categories->castAll(0);
    }
    function post( \Base $f3,$params) {
        //$data = $f3->get('POST');
        $category = (new Category());
        if($category->dry()){
            $category->copyfrom('POST',$category->fields());
            $category->save();
            $this->view->data = ['success' => true, 'message' => 'Category created Successfully'];
        }else{
            $this->view->data = ['success' => false, 'message' => 'Category not Created. Something went Wrong'];
        }

    }
    function put(\Base $f3,$params) {
         echo 'put';
    }
    function delete(\Base $f3,$params) {
         echo 'delete';
    }

} 