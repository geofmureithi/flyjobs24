<?php
/**
 * Created by PhpStorm.
 * User: Ace
 * Date: 01/11/2015
 * Time: 07:11
 */

namespace Api;


use Admin\Models\User;

abstract class Base extends \BaseController {
    function beforeroute($f3,$params){
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST))
            $_POST = json_decode(file_get_contents('php://input'), true);
        $this->view = new \View\JSON();
        $headers = getallheaders();
        $headers = str_replace('Basic ','',$headers['Authorization']);
        $headers = base64_decode($headers);
        $user= explode(':',$headers);
        $this->user = (new User())->load(array('id = ?', $user[0]));

    }
    abstract function get(\Base $f3,$params);
    abstract function post(\Base $f3,$params);
    abstract function delete(\Base $f3,$params);
    abstract function put(\Base $f3,$params);
}