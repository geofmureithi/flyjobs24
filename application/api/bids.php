<?php
/**
 * Created by PhpStorm.
 * Bid: Ace
 * Date: 01/11/2015
 * Time: 07:16
 */

namespace Api;


use Admin\Models\Bid;

class Bids extends Base {
    function get(\Base $f3,$params) {

        $bids = (new Bid())->find(array('user = ?',$this->user->id));

        if($_GET['state'] == 'pending'){
            $bids = (new Bid())->find(array('user = ? AND  bid_accepted_date IS NULL',$this->user->id));
        }

        if($_GET['state'] == 'inprogress'){
            $bids = (new Bid())->find(array('user = ? AND bid_accepted_date IS NOT NULL AND bid_completed_date IS NULL',$this->user->id));
        }

        if($_GET['state'] == 'completed'){
            $bids = (new Bid())->find(array('user = ? AND bid_completed_date IS NOT NULL',$this->user->id));
        }
        $this->view->data = [];
        if($bids)
            $this->view->data = $bids->castAll();
        if((int)$_GET['id']){
            $bid = (new Bid())->load(array('user = ? AND id = ?',$this->user->id, (int)$_GET['id']));
            if(!$bid->dry())
                $this->view->data = ['success'=>true, 'bid'=>$bid->cast(0)];
        }
    }
    function post( \Base $f3,$params) {
        //$data = $f3->get('POST');
        $bid = (new Bid());
        if($bid->dry()){
            $bid->copyfrom('POST',$bid->fields());
            $bid->active = 0;
            $bid->user = $this->user->id;
            $bid->save();
            $this->view->data = ['success' => true, 'message' => 'Bid created Successfully', 'id'=> $bid->id ];
        }else{
            $this->view->data = ['success' => false, 'message' => 'Bid not created. Something Went Wrong'];
        }

    }
    function put(\Base $f3,$params) {
        if((int)$_GET['id']){
            $bid = (new Bid())->load(array('id = ?',(int)$_GET['id']));
            $f3->set('POST',(json_decode($f3->get('BODY'),true)));
            if(!$bid->dry()){
                $bid->copyfrom('POST');
                if($bid->bid_accepted_date){
                    $bid->bid_accepted_date = (new \DateTime($bid->bid_accepted_date))->format('Y-m-d H:i:s');
                    $bid->job->bid_accepted_date = (new \DateTime($bid->bid_accepted_date))->format('Y-m-d H:i:s');
                        $bid->job->save();
                }

                if($bid->bid_declined_date)
                    $bid->bid_declined_date = (new \DateTime($bid->bid_declined_date))->format('Y-m-d H:i:s');
                $bid->save();
                $this->view->data = ['success' => true, 'message' => 'Bid Successfully Edited' ];
            }else{
                $this->view->data = ['success' => false, 'message' => 'Bid Not Found' ];
            }

        }
    }
    function delete(\Base $f3,$params) {
         echo 'delete';
    }

} 