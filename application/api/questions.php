<?php
/**
 * Created by PhpStorm.
 * Category: Ace
 * Date: 01/11/2015
 * Time: 07:16
 */

namespace Api;


use Admin\Models\Question;

class Questions extends Base {
    function get(\Base $f3,$params) {
        $Questions = (new Question())->filter('job.user',array('id = ?', $this->user->id));
        //$Questions = $Questions->filter('job.user',array('id = ?', $this->user->id));
        if($_GET['state'] == 'pending')
            $Questions = $Questions->find(array('answer IS NULL'));
        if($_GET['state'] == 'completed')
            $Questions = $Questions->find(array('answer IS NOT NULL'));
        $this->view->data = [];
        if($Questions && !(int)$_GET['id'])
            $this->view->data = $Questions->castAll();
        if((int)$_GET['id']){
            $Question = (new Question())->load(array('id = ?', (int)$_GET['id']));
            if(!$Question->dry())
                $this->view->data = ['success'=>true, 'question'=>$Question->cast()];
        }
    }
    function post( \Base $f3,$params) {
        //$data = $f3->get('POST');
        $Question = (new Question());
        if($Question->dry()){
            $Question->copyfrom('POST',$Question->fields());
            $Question->user = $this->user->id;
            $Question->save();
            $this->view->data = ['success' => true, 'message' => 'Question created Successfully'];
        }else{
            $this->view->data = ['success' => false, 'message' => 'Question not Created. Something went Wrong'];
        }

    }
    function put(\Base $f3,$params) {
        if((int)$_GET['id']){
            $Question = (new Question())->load(array('id = ?',(int)$_GET['id']));
            $f3->set('POST',(json_decode($f3->get('BODY'),true)));
            if(!$Question->dry()){
                $Question->answer= $f3->get('POST.answer');
                $Question->save();
                $this->view->data = ['success' => true, 'message' => 'Answer Successfully added' ];
            }else{
                $this->view->data = ['success' => false, 'message' => 'Question Not Found' ];
            }

        }
    }
    function delete(\Base $f3,$params) {
         echo 'delete';
    }

} 