<?php
/**
 * Created by PhpStorm.
 * Job: Ace
 * Date: 01/11/2015
 * Time: 07:16
 */

namespace Api;


use Admin\Models\Bid;
use Admin\Models\Job;

class Jobs extends Base {
    function get(\Base $f3,$params) {

        $jobs = (new Job())->find(array('user = ?',$this->user->id));
        $this->view->data = [];
        if($_GET['state'] == 'pending'){
            $jobs = (new Job())->find(array('user = ? AND  bid_accepted_date IS NULL',$this->user->id));
        }

        if($_GET['state'] == 'inprogress'){
            $jobs = (new Job())->find(array('user = ? AND bid_accepted_date IS NOT NULL AND job_finished_date IS NULL',$this->user->id));
        }

        if($_GET['state'] == 'completed'){
            $jobs = (new Job())->find(array('user = ? AND job_finished_date IS NOT NULL',$this->user->id));
        }
        if($_GET['state'] == 'open'){
            $jobs = (new Job())->find(array('user <> ? AND bid_accepted_date IS NULL',$this->user->id));
        }

        if($jobs){
            $_jobs = [];
            foreach($jobs as $job){
                $_jobs[] = (new Job)->load(array('id = ?',$job->id))->cast(null,2);
            }
            $this->view->data = $_jobs;
        }

        if((int)$_GET['id']){
            $job = (new Job())->load(array('user = ? AND id = ?',$this->user->id, (int)$_GET['id']));
            if(!$job->dry())
                $this->view->data = ['success'=>true, 'job'=>$job->cast(null, 3)];
        }
        if((int)$_GET['job']){
            $job = (new Job())->load(array('id = ?',(int)$_GET['job']));
            if(!$job->dry())
                $this->view->data = ['success'=>true, 'job'=>$job->cast()];
        }
        if($_GET['usercanbid']=='true'){
            $bid = new Bid();
            $bid = $bid->load(array('job = ? AND user = ? AND (bid_declined_date IS NULL OR bid_accepted_date IS NULL) AND bid_completed_date is NULL', (int)$_GET['job'],$this->user->id));
            if($bid->dry()){
                $this->view->data = ['success'=>true,'message'=>'Bid can be Created'];
            }else{
                $this->view->data = ['success'=>false,'message'=>'Bid cannot be Created! The client has not responded to your previous bid'];
            }
        }
    }
    function post( \Base $f3,$params) {
        //$data = $f3->get('POST');
        $job = (new Job());
        if($job->dry()){
            $job->copyfrom('POST',$job->fields());
            $job->active = 0;
            $job->user = $this->user->id;
            $job->save();
            $this->view->data = ['success' => true, 'message' => 'Job created Successfully', 'id'=> $job->id ];
        }else{
            $this->view->data = ['success' => false, 'message' => 'Job not created. Something Went Wrong'];
        }

    }
    function put(\Base $f3,$params) {
         echo 'put';
    }
    function delete(\Base $f3,$params) {
         echo 'delete';
    }

} 