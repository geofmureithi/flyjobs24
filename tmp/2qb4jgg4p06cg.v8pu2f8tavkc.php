
<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="/assets/ico/favicon.png">
    <base href="/">
<title><?php echo @$app['title']; ?> | Login</title>
<!-- Bootstrap core CSS -->
<link href="/assets/bootstrap/css/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/assets/css/style.css" rel="stylesheet">

<!-- styles needed for carousel slider -->
<link href="/assets/css/owl.carousel.css" rel="stylesheet">
<link href="/assets/css/owl.theme.css" rel="stylesheet">

<!-- Just for debugging purposes. -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<!-- include pace script for automatic web page progress bar  -->

<script>
    paceOptions = {
      elements: false,
        eventLag: true,
        restartOnPushState: true,
        restartOnRequestAfter: true,
        ajax: {
            trackMethods: ['GET', 'POST', 'PUT','DELETE'],
            trackWebSockets: false
        }

    };
</script>
<script src="/assets/js/pace.min.js"></script>

</head>
<body>
<div id="wrapper">
  <div class="header">
    <nav class="navbar   navbar-site navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a href="index.html" class="navbar-brand logo logo-title"> 
          <!-- Original Logo will be placed here  --> 
          <span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo shape-0"></i> </span> Fly<span>Jobs24 </span> </a> </div>
        <div class="navbar-collapse collapse">
          
          <ul class="nav navbar-nav navbar-right" ng-show="!isLoggedin">
            <li><a href="/#/login">Login</a></li>
            <li><a href="/#/register">Signup</a></li>
            <li class="postadd"><a class="btn btn-block   btn-border btn-post btn-danger" href="post-ads.html">Post Free Add</a></li>
          </ul>
            <ul class="nav navbar-nav navbar-right" ng-show="isLoggedin">
                <li><a href="#">Signout <i class="glyphicon glyphicon-off"></i> </a></li>
                <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown"> <span><?php echo currentUser.user.firstname; ?></span> <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
                    <ul class="dropdown-menu user-menu">
                        <li class="active"><a href="account-home.html"><i class="icon-home"></i> Personal Home </a></li>

                        <li><a href="account-myads.html"><i class="icon-th-thumb"></i> My ads </a></li>
                        <li><a href="account-favourite-ads.html"><i class="icon-heart"></i> Favourite ads </a></li>
                        <li><a href="account-saved-search.html"><i class="icon-star-circled"></i> Saved search </a></li>
                        <li><a href="account-archived-ads.html"><i class="icon-folder-close"></i> Archived ads </a></li>
                        <li><a href="account-pending-approval-ads.html"><i class="icon-hourglass"></i> Pending approval </a></li>
                        <li><a href="statements.html"><i class=" icon-money "></i> Payment history </a></li>
                    </ul>
                </li>
                <li class="postadd"><a class="btn btn-block   btn-border btn-post btn-danger" href="post-ads.html">Post Free Add</a></li>
            </ul>

        </div>
        <!--/.nav-collapse --> 
      </div>
      <!-- /.container-fluid --> 
    </nav>
  </div>
  <!-- /.header -->

  <div ng-view></div>
  
  <div class="footer" id="footer">
    <div class="container">
      <ul class=" pull-left navbar-link footer-nav">
        <li><a href="index.html"> Home </a> <a href="about-us.html"> About us </a> <a href="#"> Terms and Conditions </a> <a href="#"> Privacy Policy </a> <a href="contact.html"> Contact us </a> <a href="faq.html"> FAQ </a>
      </ul>
      <ul class=" pull-right navbar-link footer-nav">
        <li> &copy; 2015 <?php echo $app['title']; ?> </li>
      </ul>
    </div>
    
  </div>
  <!-- /.footer -->
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 

<!-- Placed at the end of the document so the pages load faster --> 
    <script src="//code.jquery.com/jquery-2.0.3.min.js"></script>
    <script src="//code.angularjs.org/1.2.20/angular.js"></script>
    <script src="//code.angularjs.org/1.2.20/angular-route.js"></script>
    <script src="//code.angularjs.org/1.2.13/angular-cookies.js"></script>
	<script src="/assets/bootstrap/js/bootstrap.min.js"></script> 
	<!-- include Angular Files  --> 
	<script src="/scripts/app.js"></script> 
	<script src="/scripts/services/auth/registerservice.js"></script> 
	<script src="/scripts/services/auth/authservice.js"></script> 
	<script src="/scripts/services/flash/flashservice.js"></script> 
	<script src="/scripts/services/users/service.js"></script> 
	<script src="/scripts/controllers/login.js"></script> 
	<script src="/scripts/controllers/forgotpassword.js"></script>
	<script src="/scripts/controllers/home.js"></script>
	<script src="/scripts/controllers/register.js"></script> 

<!-- include carousel slider plugin  --> 
<script src="/assets/js/owl.carousel.min.js"></script> 

<!-- include form-validation plugin || add this script where you need validation   --> 
<script src="/assets/js/form-validation.js"></script> 

<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js"></script>
<script src="/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js"></script>
<script src="/assets/js/hideMaxListItem-min.js"></script>
<!-- include custom script for site  --> 
<script src="/assets/js/script.js"></script>
</body>
</html>
