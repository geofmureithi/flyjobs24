<?php

// Kickstart the framework
$f3=require('lib/base.php');


$f3->set('DEBUG',0);
if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');


// Load configuration
$f3->config('config.ini');
\Template\FooForms::init();
$f3->set('AppDB1', new \DB\SQL('mysql:host=localhost;port=3306;dbname='. $f3->get('mysql.db'), $f3->get('mysql.username'), $f3->get('mysql.password')));
$f3->route('GET|POST /auth/@action','\Frontend\Controllers\Auth->@action');
$f3->map('/api/@item','\Api\@item');
$f3->route('GET|POST /', function($f3){
	
	$f3->set('UI','application/frontend/views/');
	echo \View::instance()->render('layout.html');
});
$f3->route('GET|POST /*', function(\Base $f3,$params){
    $f3->reroute('/#'.$params[0]);
});
$f3->route('POST /misc/file-details',function($f3){
    $myFS = new \FAL\LocalFS('files/');
    $details = new stdClass();
    $filename = $f3->get('POST.filename');
    if($myFS->exists($filename)){
        $details->name= basename($filename);
        $details->path = '/files/'.$filename;
        $details->size = $myFS->size($filename);
    }
    echo json_encode(array($details));
});

$f3->route('GET /models/setup',
    function($f3){
        $models = array('User','Job','Category', 'Bid', 'Question');
        foreach($models as $model){
            $model = '\Admin\Models\\' . $model;
            $x = (new $model);
            $x::install();
        }
    });

$f3->run();