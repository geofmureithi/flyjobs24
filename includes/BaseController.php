<?php

class BaseController {
    public $view;
    protected $limit = 10;
    function beforeroute($f3,$params) {
		//code here;
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST))
            $_POST = json_decode(file_get_contents('php://input'), true);
        \Admin\Models\Setting::reload($f3); // Load saved settings
        $this->page = \Pagination::findCurrentPage();
        $this->option = array('order' => 'date_created DESC');
        $this->filter = null;
        $ajax = true;
        if(!$ajax) {
            $this->view = new \View\Html();
        } else {
            $this->view = new \View\JSON();
        }
	}
	function afterroute($f3,$params) {
		//code here;

        echo $this->view->render();
	}
}