<?php

class BaseModel extends \DB\Cortex {
	protected
		$db = 'AppDB1',     // F3 hive key of a valid DB object
        $fluid = true,
        $ttl = 0,
		$table = '';


	static function install(){
		static::setup();
	}
	function __construct(){
		parent::__construct();
		$this->beforeinsert(function($self){
			$self->creator = \Base::instance()->get('SESSION.user_id');
		});

	}
    static function select_data($show="title",$key="id"){
        $instance = new static;
        $data = $instance->find();
        //var_dump($data);
        $final_data = [];
        if($data)
        foreach($data as $k=>$d)
            $final_data[$d[$key]] =  $d[$show];
        return $final_data;
    }
    function delete($id){
        return $this->load(array('id = ?'),$id)->erase();
    }

    public function set_title($value) {
        $this->slug = \Web::instance()->slug($value);
        return $value;
    }
    static function countAll(){
        return (new static)->count();
    }
    static function status($active){
        $instance = new static;
        $data = $instance->find(array('active = ?', (int)$active));
        if($data)
            return count(@$data->castAll());
        return 0;

    }


}