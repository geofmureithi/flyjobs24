<?php
/**
 * Created by PhpStorm.
 * User: Ace
 * Date: 12/08/2015
 * Time: 12:29
 */

class Translator {
    private $web;
    protected $services = array('yandex'=>'https://translate.yandex.net/api/v1.5/tr.json/','google', 'microsoft');
    protected $default = 'yandex';
    function __construct(){
        $this->web = \Web::instance();
        $default = \Base::instance()->get('TRANSLATOR.default');
        $this->default = isset($default)?$default:$this->default;
    }

} 