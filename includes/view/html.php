<?php
/**
 * Created by PhpStorm.
 * User: Ace
 * Date: 30/07/2015
 * Time: 16:00
 */

namespace View;


class Html {
    protected
        $template = 'layout.html';
    public function setTemplate($filepath) {
        $this->template = $filepath;
    }
    public function render() {
        // add template data to F3 hive
        if($this->data)
            \Base::instance()->mset($this->data);
        // render base layout, the rest happens in template
        return \Template::instance()->render($this->template);
    }

} 