<?php
/**
 * Created by PhpStorm.
 * User: Ace
 * Date: 30/07/2015
 * Time: 15:58
 */
namespace View;

abstract class Base {

    public $data = array();

    /**
     * create and return response content
     * @return mixed
     */
    abstract public function render();

}