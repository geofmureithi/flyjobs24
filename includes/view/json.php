<?php
/**
 * Created by PhpStorm.
 * User: Ace
 * Date: 30/07/2015
 * Time: 15:57
 */
namespace View;

class JSON extends Base {
    public $data;
    public function render() {
        header('Content-Type: application/json');
        return json_encode($this->data);
    }
    public function setTemplate(){

    }

}